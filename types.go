package main

type Object interface {
	exec() Object
}

type Ref struct {
	ref string
}

type Map struct {
	map_ map[Ref]Object
}

type Call struct {
	ref  Ref
	map_ Map
}

type Text struct {
	rawText RawText
	fmtText FmtText
}

type RawText struct {
	objs []Object
}

type FmtText struct {
	objs []Object
}

type Block struct {
	objs []Object
}

type BlankObject struct{}

func (bObj BlankObject) exec() Object {
	return nil
}

func NewBlankObject() Object {
	return BlankObject{}
}
