package main

import "log"

func NewBlock(objs ...Object) Block {
	//for _, obj := range objs {
	//	obj.exec()
	//}
	log.Print("Block")
	log.Print(objs)
	return Block{
		objs: objs,
	}
}

func NewRef(rawRefs string) Ref {
	//for _, obj := range objs {
	//	obj.exec()
	//}
	log.Print("Ref")
	log.Print(rawRefs)
	return Ref{
		ref: rawRefs,
	}
}

func NewCall(ref Ref, map_ Map) Call {
	//for _, obj := range objs {
	//	obj.exec()
	//}
	log.Print("Call")
	log.Print(ref)
	log.Print(map_)
	return Call{
		ref:  ref,
		map_: map_,
	}
}

func NewMap(map_ map[Ref]Object) Map {
	//for _, obj := range objs {
	//	obj.exec()
	//}
	log.Print("Map")
	log.Print(map_)
	return Map{
		map_: map_,
	}
}

func NewText(rawText RawText, fmtText FmtText) Text {
	//for _, obj := range objs {
	//	obj.exec()
	//}
	log.Print("Text")
	log.Print(rawText)
	log.Print(fmtText)
	return Text{
		rawText: rawText,
		fmtText: fmtText,
	}
}

func NewRawText(objs ...Object) RawText {
	//for _, obj := range objs {
	//	obj.exec()
	//}
	log.Print("RawText")
	log.Print(objs)
	return RawText{
		objs: objs,
	}
}

func NewFmtText(objs ...Object) FmtText {
	//for _, obj := range objs {
	//	obj.exec()
	//}
	log.Print("FmtText")
	log.Print(objs)
	return FmtText{
		objs: objs,
	}
}
